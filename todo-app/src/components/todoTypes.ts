export interface todoItem {
  id: number;
  user: number;
  text: string;
  completed: boolean;
}
